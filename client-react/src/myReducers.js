import React, { useReducer } from 'react'
import { graphql } from 'react-apollo'
// loadash
import { flowRight as compose } from 'lodash'

import { deleteServiceMutation } from './queries/mutations'

initialState = {}

const reducer = (state, action) => {
    switch (action.type) {
        case 'removeService':
            return {
                removeService: (payload) => {
                    deleteServiceMutation({
                        variables: payload
                    })
                }
            }
        default:
            return state
    }
}

function myReducers(props) {
    console.log(props)

    const [removeService, dispatch] = useReducer(reducer)

    return (
        <div>

        </div>
    )
}

export default compose(
    graphql(deleteServiceMutation, { name: 'deleteServiceMutation' })
)(myReducers)
