import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { ColorContext } from '../ColorContext'

// components
import SignupProgress from '../components/SignupProgress'
import SignUpSideBanner from '../components/SignUpSideBanner'
import ButtonCta from '../components/ButtonCta'

const SignupPetDetails = (props) => {
    const [colors] = useContext(ColorContext)

    const SaveExitContainer = styled.div`
        position: relative;
        
        a:link,
        a:visited,
        a:hover,
        a:active {
            text-decoration: none;
            color: white;
        }

        .save-exit {
            position: absolute;
            bottom: 40px;
            right: 50%;
            padding: 0;
            transform: translate(50%, -50%);
        }
    `

    const PetType = styled.div`
        background: ${colors.tertiaryLight};
    `

    return (
        <React.Fragment>
            <PetType>
                <div className="row">
                    <SaveExitContainer className="col-12 col-md-4 p-0">
                        <SignUpSideBanner
                            photo="/images/cat_01.png"
                            content={SignupProgress("pet details")}
                        />

                        <Link to='/' >
                            <a className="save-exit"> Save and Exit</a>
                        </Link>

                    </SaveExitContainer>
                    <div className="col-12 col-md-8 p-0 text-center">
                        <h2 className="w-75 mx-auto  my-5">Thanks! Now give us all the details about Ginger.</h2>

                        <form enctype="multipart/form-data" className="w-75 mx-auto">
                            <p align="left">Favorites Things</p>
                            <div className="row">
                                <div className="col-12 col-md-4">
                                    <div className="form-group">
                                        <input type="checkbox" id="selectAll" className="mr-2" />
                                        <label htmlFor="selectAll">Select All</label>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="form-group">
                                        <input type="checkbox" id="givingKisses" className="mr-2" />
                                        <label htmlFor="givingKisses">Giving Kisses</label>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="form-group">
                                        <input type="checkbox" id="walks" className="mr-2" />
                                        <label htmlFor="walks">Walks</label>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12 col-md-4">
                                    <div className="form-group">
                                        <input type="checkbox" id="barking" className="mr-2" />
                                        <label htmlFor="barking">Barking</label>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="form-group">
                                        <input type="checkbox" id="snuggling" className="mr-2" />
                                        <label htmlFor="snuggling">Snuggling</label>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="form-group">
                                        <input type="checkbox" id="treats" className="mr-2" />
                                        <label htmlFor="treats">Treats</label>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12 col-md-4">
                                    <div className="form-group">
                                        <input type="checkbox" id="playingFetch" className="mr-2" />
                                        <label htmlFor="playingFetch">Playing Fetch</label>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="form-group">
                                        <input type="checkbox" id="naps" className="mr-2" />
                                        <label htmlFor="naps">Naps</label>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="form-group">
                                        <input type="checkbox" id="toys" className="mr-2" />
                                        <label htmlFor="toys">Toys</label>
                                    </div>
                                </div>
                            </div>

                            <div className="input-group">
                                <label htmlFor="food">Food</label>
                                <textarea
                                    name="food"
                                    cols="30"
                                    rows="3"
                                    id="food"
                                    className="form-control"
                                    placeholder="Tell us the what, when, and where of food."></textarea>
                            </div>
                            <div className="input-group mt-4">
                                <label htmlFor="others">Anything else</label>
                                <textarea
                                    name="others"
                                    cols="30"
                                    rows="3"
                                    id="others"
                                    className="form-control"
                                    placeholder="Hopes and dreams, biggest fears, medical issues, etc."></textarea>
                            </div>
                        </form>

                        <div className="row mt-5">
                            <div className="col-12 col-8">
                                <div className="signup-footer d-md-flex justify-content-around align-items-center w-75 mx-auto">
                                    <Link to="/signup/pet-type">
                                        <ButtonCta
                                            title="Back"
                                            color={colors.greyLight}
                                            bgColor={colors.primary}
                                            colorHover={colors.primaryLight}
                                        />
                                    </Link>

                                    <Link to="/signup/vet">
                                        <ButtonCta
                                            title="Next"
                                            color={colors.primary}
                                            bgColor={colors.grey}
                                            colorHover={colors.greyLight}
                                        />
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </PetType>
        </React.Fragment>
    )
}

export default SignupPetDetails