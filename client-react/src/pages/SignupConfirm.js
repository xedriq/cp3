import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'


//context
import { ColorContext } from '../ColorContext'

// components
import SignupProgress from '../components/SignupProgress'
import SignUpSideBanner from '../components/SignUpSideBanner'
import ButtonCta from '../components/ButtonCta'
import PetProfile from '../components/pet-profile/PetProfile'



const SignupConfirm = () => {
    const [colors] = useContext(ColorContext)

    const SaveExitContainer = styled.div`
        position: relative;
        
        a:link,
        a:visited,
        a:hover,
        a:active {
            text-decoration: none;
            color: white;
        }

        .save-exit {
            position: absolute;
            bottom: 40px;
            right: 50%;
            padding: 0;
            transform: translate(50%, -50%);
        }
    `

    const PetType = styled.div`
        background: ${colors.tertiaryLight};
    `
    let pet = JSON.parse(localStorage.getItem('pet'))

    return (
        <React.Fragment>
            <PetType>
                <div className="row">
                    <SaveExitContainer className="col-12 col-md-4">
                        <SignUpSideBanner
                            photo="/images/puppy_01.png"
                            content={SignupProgress("confirm")}
                        />

                        <Link to='/' >
                            <a className="save-exit">Save and Exit</a>
                        </Link>

                    </SaveExitContainer>
                    <div className="col-12 col-md-8 text-center">
                        <h3 className="w-75 mx-auto my-5">Okay, {pet.name} is all set!
                            We can’t wait to meet her.</h3>
                        <div className="w-75 mx-auto">
                            <PetProfile petData={pet} />
                        </div>
                        <p className="w-75 mx-auto my-4">Got more pets? Lucky you! <Link to="#">  Add another pet profile</Link></p>

                        <div className="row mt-5">
                            <div className="col-12 col-8">
                                <div className="signup-footer d-md-flex justify-content-md-between align-items-md-center w-75 mx-auto">
                                    <Link to="/signup/pet-type">
                                        <ButtonCta
                                            title="Back"
                                            color={colors.greyLight}
                                            bgColor={colors.primary}
                                            colorHover={colors.primaryLight}
                                        />
                                    </Link>

                                    <Link to="/dashboard">
                                        <ButtonCta
                                            title="Confirm"
                                            color={colors.primary}
                                            bgColor={colors.grey}
                                            colorHover={colors.greyLight}
                                        />
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </PetType>
        </React.Fragment>
    )
}

export default SignupConfirm