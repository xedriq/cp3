import React, { useContext, useState } from 'react'
import { Link, Redirect } from 'react-router-dom'
import styled from 'styled-components'
import { ColorContext } from '../ColorContext'

import { graphql } from 'react-apollo'

// loadash
import { flowRight as compose } from 'lodash'

import Swal from "sweetalert2";


// components
import SignupProgress from '../components/SignupProgress'
import SignUpSideBanner from '../components/SignUpSideBanner'
// import LargeSelect from '../components/large-select/LargeSelect'
import ButtonCta from '../components/ButtonCta'

// context
import { UserContext } from '../App'

//mutation
import { createPetMutation } from '../queries/mutations'

const SignupPetType = (props) => {
    //hooks
    const [colors] = useContext(ColorContext)
    // const [user] = useContext(UserContext)
    const [user] = useState(JSON.parse(localStorage.getItem('user')))
    const [petType, setPetType] = useState('')
    const [userId] = useState(user.id)
    const [gotoPetBasicSignup, setGotoPetBasicSignup] = useState(false)


    // console.log(userId)
    // console.log(createUser)
    // let {createUser} = user
    // console.log(user.id)

    const SaveExitContainer = styled.div`
        position: relative;
        
        a:link,
        a:visited,
        a:hover,
        a:active {
            text-decoration: none;
            color: white;
        }

        .save-exit {
            position: absolute;
            bottom: 40px;
            right: 50%;
            padding: 0;
            transform: translate(50%, -50%);
        }
    `
    // useEffect(()=>{
    //     setPet({id:localStorage.getItem('pet_id')
    // })},[])

    const PetType = styled.div`
        background: ${colors.tertiaryLight};
    `

    const handleChange = (e) => {
        setPetType(e.target.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault()

        props.createPetMutation({
            variables: { owner_id: userId, pet_type: petType },
            // refetchQueries:[]
        }).then((res) => {
            Swal.fire({
                icon: 'success',
                title: 'Pet created!'
            })
            console.log(res.data)

            localStorage.setItem('pet_id', String(res.data.createPet.id))
            localStorage.setItem('pet_type', String(res.data.createPet.pet_type))

            setGotoPetBasicSignup(true)
        }).catch(error => {
            Swal.fire({
                icon: "error",
                title: String(error.message)
            });
        })
    }

    if (gotoPetBasicSignup) {
        return <Redirect to='/signup/pet-basics' />
    }

    return (
        <React.Fragment>
            <PetType>
                <div className="row">
                    <SaveExitContainer className="col-12 col-md-4 p-0">
                        <SignUpSideBanner
                            photo="/images/signup_image_3.png"
                            content={SignupProgress("pet type")}
                        />

                        <Link to='/' >
                            <p className="save-exit"> Save and Exit</p>
                        </Link>

                    </SaveExitContainer>
                    <div className="col-12 col-md-8 px-0 pt-5 text-center">
                        <h2 className="w-75 mx-auto  my-5">Nice to meet you, {user.first_name}.
                        Tell us all about your furry,
                        feathery, or scaley friend.</h2>

                        <h5 className="w-50 mx-auto  mb-4">What type of pet do you have?</h5>
                        <form onSubmit={handleSubmit} >
                            <div className="form-group">
                                <input
                                    onChange={handleChange}
                                    className="mr-2"
                                    type="radio"
                                    id="dog"
                                    value="dog"
                                    name="petType"
                                    checked={petType === "dog" && true} />
                                <label htmlFor="dog">Dog</label>
                            </div>
                            <div className="form-group">
                                <input
                                    onChange={handleChange}
                                    className="mr-2"
                                    type="radio"
                                    id="cat"
                                    value="cat"
                                    name="petType"
                                    checked={petType === "cat" && true} />
                                <label htmlFor="cat">Cat</label>
                            </div>
                            <div className="form-group">
                                <input
                                    onChange={handleChange}
                                    className="mr-2"
                                    type="radio"
                                    id="bird"
                                    value="bird"
                                    name="petType"
                                    checked={petType === "bird" && true} />
                                <label onChange={handleChange} htmlFor="bird">Bird</label>
                            </div>
                            <div className="form-group">
                                <input
                                    onChange={handleChange}
                                    className="mr-2"
                                    type="radio"
                                    id="others"
                                    value="others"
                                    name="petType"
                                    checked={petType === "others" && true} />
                                <label htmlFor="others">Others</label>
                            </div>

                            <p className="w-50 mx-auto my-4">Have multiple pets? That’s awesome. You can create additional pet profiles for the whole family later.</p>

                            <div className="row mt-5">
                                <div className="col-12 col-8">
                                    <div className="signup-footer d-md-flex justify-content-around align-items-center w-75 mx-auto">
                                        <Link to="/signup/user">
                                            <ButtonCta
                                                title="Back"
                                                color={colors.greyLight}
                                                bgColor={colors.primary}
                                                colorHover={colors.primaryLight}
                                            />
                                        </Link>


                                        <ButtonCta
                                            title="Next"
                                            color={colors.primary}
                                            bgColor={colors.grey}
                                            colorHover={colors.greyLight}
                                        />

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </PetType>
        </React.Fragment>
    )
}

export default compose(
    graphql(createPetMutation, { name: "createPetMutation" })
)(SignupPetType)