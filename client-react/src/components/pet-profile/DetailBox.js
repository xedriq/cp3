import React, { useState } from 'react'
import styled from 'styled-components'

import Moment from 'react-moment'
import moment from 'moment'

const DetailBoxDiv = styled.div`
    display: flex;
    border: 1px solid lightgrey;
    border-radius: 5px;
    justify-content:center;
    width: 480px;
    margin: 0 auto;
    text-align: center;
    p {
        margin: 0;
    }
`
const DetailDiv = styled.div`
    padding:  1em;
    // margin: 0 auto;
    width: 120px;

    &:not(:last-child) {
        border: 1px  lightgrey;
        border-style: none solid none none;
    }
`

const IconDiv = styled.div`
    height:40px;
    width:40px;
    background-image: url("${props => props.iconImage}");
    background-size: cover;
    margin: 0 auto;
`

const DetailBox = ({ petData }) => {
    const [today, setToday] = useState(new Date())

    let age = moment(new Date()).format("YYYY") - moment(petData.birthday).format("YYYY");

    return (
        <DetailBoxDiv>
            <DetailDiv>
                <IconDiv iconImage={petData.gender === 'male' ? "/images/icon_male.png" : "/images/icon_female.png"} />
                <p>{petData.gender}</p>
            </DetailDiv>

            <DetailDiv>
                <h4>{age}</h4>
                <p>years old</p>
            </DetailDiv>

            <DetailDiv>
                <IconDiv iconImage="/images/icon_check.png" />
                <p>{petData.spayed_neutered ? "Spayed" : "Normal"}</p>
            </DetailDiv>

            <DetailDiv>
                <h4>{petData.weight}</h4>
                <p>kgs</p>
            </DetailDiv>
        </DetailBoxDiv>
    )
}

export default DetailBox