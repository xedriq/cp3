import React from 'react'
import styled from 'styled-components'

const ImageCircle = (props) => {

    const ImageCircleDiv = styled.div`
        position: ${props => props.attrib.position};
        top: ${props => props.attrib.top};
        left: ${props => props.attrib.left};
        transform: translate(-50%, -50%);
        width: 80px;
        height: 80px;
        background-image: url("${props => props.petPhoto}");
        background-size: cover;
        background-position: center;
        border-radius: 50%;
        border: 1px solid lightgrey;
    `

    return (
        <ImageCircleDiv
            petPhoto={props.petPhoto}
            attrib={props.attrib}
        />
    )
}

export default ImageCircle