import React from 'react'
import styled from 'styled-components'

const ButtonCtaButton = styled.button`
	height: 56px;
	width: 256px;
	border-radius: 30px;
	background: ${props => props.bgColor};
	color: ${props => props.color};
	border: none;
	box-shadow: 3px 3px 15px rgba(33,35,44, .2);

	:focus {
		outline: none;
	}

	:hover {
		background: ${props => props.colorHover};
	}
`

const ButtonCta = (props) => {

	return (
		<React.Fragment>
			<ButtonCtaButton type={props.type === "button" ? "button" : "submit"} bgColor={props.bgColor} color={props.color} colorHover={props.colorHover} >
				{props.title}
			</ButtonCtaButton>
		</React.Fragment>
	)
}

export default ButtonCta