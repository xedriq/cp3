import React from 'react'
import styled from 'styled-components'

const Footer = () => {

    const FooterSection = styled.section`
        display: absolute;
        bottom: 0;
        background: #F7ECEB;
        padding: 4em;
        text-align: center;
    `

    return (
        <FooterSection>
            <h1>Contact</h1>
            <h2>481-624-3240</h2>
            <h2>Email Us</h2>
        </FooterSection>
    )
}

export default Footer