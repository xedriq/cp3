import React, { createContext, useState } from 'react'

export const ColorContext = createContext()

export const ColorContextProvider = (props) => {

    const [colors, setColors] = useState({
        primary: "#545871",
        primaryLight: "#9597A6",
        grey: "#DADBE6",
        greyLight: "#F0F1F7",
        secondary: "#B8817D",
        secondaryLight: "#EBD0CE",
        tertiary: "#E6DBDA",
        tertiaryLight: "#F7ECEB"
    })

    return (
        <ColorContext.Provider value={[colors, setColors]}  >
            {props.children}
        </ColorContext.Provider>
    )
}