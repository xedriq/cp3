// const { GraphQLDateTime } = require('graphql-iso-date')
const mongoose = require('mongoose')

const Schema = mongoose.Schema



const petSchema = new Schema({

    name: {
        type: String,
        // required: true
    },
    owner_id: {
        type: String,
        required: true
    },
    owner: {
        type: String,

    },
    birthday: {
        type: mongoose.Schema.Types.Date
    },
    pet_type: {
        type: String,
        required: true
    },
    breed: {
        type: String,
        // required: true
    },
    photo: {
        type: String,
    },
    gender: {
        type: String,
        // required: true
    },
    spayed_neutered: {
        type: Boolean,
        // required: true
    },
    weight: {
        type: Number,
    },
    favorites: {
        type: Array,
    },
    vet_id: {
        type: String
    },
    vet: {
        type: String
    },
    food: {
        type: String
    },
    others: {
        type: String
    },
    service_id: {
        type: String
    }
}, {
    timestamps: true
})

module.exports = mongoose.model('Pet', petSchema)