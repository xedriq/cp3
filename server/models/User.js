const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new Schema({
    first_name: {
        type: String,
        // required: true
    },
    last_name: {
        type: String,
        // required: true
    },
    phone: {
        type: String,
    },
    alt_phone: {
        type: String,
    },
    street_address: {
        type: String,
    },
    city: {
        type: String,
        // required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    pet_ids: {
        type: Array
    },
    pets: {
        type: Array
    },
    services: {
        type: Array
    },
    service_ids: {
        type: Array
    },
    user_type: {
        type: String,
        
    }
}, {
    timestamps: true
})

module.exports = mongoose.model('User', userSchema)