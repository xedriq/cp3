const mongoose = require('mongoose')
const Schema = mongoose.Schema

const vetSchema = new Schema({
    first_name: {
        type: String,
        required: true
    },
    last_name: {
        type: String,
        required: true
    },
    phone: {
        type: Number,
        required: true
    },
    alt_phone: {
        type: Number,
    },
    street_address: {
        type: String,
        required: true
    },
    city: {
        type: String,
        required: true
    }
}, {
    timestamps: true
})

module.exports = mongoose.model('Vet', vetSchema)