// backend server
// declare and use the dependencies

// use/import the express library
const express = require("express")

// instantiate an express project
// serve our project using express
const app = express();

// use the mongoose library
const mongoose = require("mongoose");

// database connection
mongoose.connect(
    "mongodb+srv://xedriq:xedriq@cluster0-saupb.mongodb.net/pawtastic_db?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    }
)

mongoose.connection.once("open", () => {
    console.log("Connected to MongoDB server...")
})


//  import the instantiation of the apollo server
const server = require("./queries/queries.js");


// the app will be served by the apollo server instead 
// of express
server.applyMiddleware({
    app,
    // path: "/batch43"
})

// server initialization
app.listen(4000, () => {
    console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`);
})

